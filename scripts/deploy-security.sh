#!/bin/bash

#######################################################################
#      VALIDATION OF COMMAND LINE INTERFACES IN THE ENVIRONMENT       #
#######################################################################

# VALIDATE IF AWS CLI IS INSTALLED
if ! [ -x "$(command -v aws)" ]; then
  date=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date [ ERROR ] aws cli not found"
  exit 1
fi

# VALIDATE IF SAM CLI IS INSTALLED
if ! [ -x "$(command -v sam)" ]; then
  date=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date [ ERROR ] sam cli not found"
  exit 1
fi

#######################################################################
#             ENVIRONMENT VARIABLES FOR COMMAND EXECUTION             #
#######################################################################

## DEPLOYMENT ENVIRONMENT
ENV="dev"

## USE THE ONE IN ~/.aws/credentials
AWS_PROFILE="COMAFI_SANDBOX_ACCOUNT"

## NAME OF THE LOOP IN S3 WHERE THE CODE WILL BE UPLOADED, THIS MUST EXIST BEFORE EXECUTING THE FILE
BUCKET="bucket-sam-deploy-sandbox"

STACK="Markdown"
PROJECT=${STACK}
ROOT_PATH="$(pwd)"
SOURCE="$ROOT_PATH/cloudformation"
BUILD_PATH="$ROOT_PATH/.aws-sam/build/"
UUID=$$

#######################################################################
#     CLOUDFORMATION VALIDATION:: cloudformations/security.yaml       #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: VALIDATING TEMPLATE ${SOURCE}/security.yaml"
sam validate \
  --template "${SOURCE}/security.yaml" \
  --profile ${AWS_PROFILE}

#######################################################################
#   CLOUDFORMATION BUILDING PROCESS:: cloudformations/security.yaml   #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: BUILDING SAM ${SOURCE}/security.yaml"
sam build -t "${SOURCE}/security.yaml"

#######################################################################
#                     PROJECT PACKAGING PROCESS                       #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: PACKAGING SAM PROJECT"
sam package \
  --profile ${AWS_PROFILE} \
  --template-file "${BUILD_PATH}/template.yaml" \
  --output-template-file "security_${UUID}.yaml" \
  --s3-bucket ${BUCKET}

#######################################################################
#                   AWS PROJECT DEPLOYMENT PROCESS                    #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: DEPLOYING SAM PROJECT"
sam deploy \
  --profile ${AWS_PROFILE} \
  --template-file "security_${UUID}.yaml" \
  --stack-name "${ENV}-${STACK}-Security" \
  --capabilities CAPABILITY_NAMED_IAM \
  --tags Project=${PROJECT} \
  --parameter-overrides \
  Environment=${ENV} \
  Project=${PROJECT}

#######################################################################
# LIMPIEZA DE PRECESO DE DESPLIEGUE
#######################################################################

rm "security_${UUID}.yaml"
rm -rf "$(pwd)/.aws-sam"
