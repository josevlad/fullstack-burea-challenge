#!/bin/bash

#######################################################################
#      VALIDATION OF COMMAND LINE INTERFACES IN THE ENVIRONMENT       #
#######################################################################

# VALIDATE IF AWS CLI IS INSTALLED
if ! [ -x "$(command -v aws)" ]; then
  date=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date [ ERROR ] aws cli not found"
  exit 1
fi

# VALIDATE IF SAM CLI IS INSTALLED
if ! [ -x "$(command -v sam)" ]; then
  date=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date [ ERROR ] sam cli not found"
  exit 1
fi

# VALIDATE IF NPM CLI IS INSTALLED
if ! [ -x "$(command -v npm)" ]; then
  date=$(date '+%Y-%m-%d %H:%M:%S')
  echo "$date [ ERROR ] npm not found"
  exit 1
fi

#######################################################################
#             ENVIRONMENT VARIABLES FOR COMMAND EXECUTION             #
#######################################################################

## DEPLOYMENT ENVIRONMENT
ENV="dev"

## USE THE ONE IN ~/.aws/credentials
AWS_PROFILE="COMAFI_SANDBOX_ACCOUNT"

## NAME OF THE LOOP IN S3 WHERE THE CODE WILL BE UPLOADED, THIS MUST EXIST BEFORE EXECUTING THE FILE
BUCKET="bucket-sam-deploy-sandbox"

STACK="Markdown"
PROJECT=${STACK}
ROOT_PATH="$(pwd)"
SOURCE="$ROOT_PATH/cloudformation"
BUILD_PATH="$ROOT_PATH/.aws-sam/build/"
LOG_LEVEL="debug"
LOG_RETENTION_IN_DAYS=5
UUID=$$
REPOSITORY="https://gitlab.com/josevlad/fullstack-burea-challenge"

#######################################################################
#     CLOUDFORMATION VALIDATION:: cloudformations/template.yaml       #
#######################################################################


date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: VALIDATING TEMPLATE ${SOURCE}/template.yaml"
sam validate \
  --template "${SOURCE}/template.yaml" \
  --profile ${AWS_PROFILE}


#######################################################################
#   CLOUDFORMATION BUILDING PROCESS:: cloudformations/template.yaml   #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: BUILDING SAM ${SOURCE}/template.yaml"
sam build -t "${SOURCE}/template.yaml"

########################################################################
##                    INSTALLING LAYER DEPENDENCIES                    #
########################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo -e "$date [ INFO ] :: INSTALLING LAYER DEPENDENCIES"

node "$ROOT_PATH/src/back/layer/npm-install.js"

########################################################################
##                     PROJECT PACKAGING PROCESS                       #
########################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: PACKAGING SAM PROJECT"
sam package \
  --profile ${AWS_PROFILE} \
  --template-file "${BUILD_PATH}/template.yaml" \
  --output-template-file "template_${UUID}.yaml" \
  --s3-bucket ${BUCKET}

#######################################################################
#                   AWS PROJECT DEPLOYMENT PROCESS                    #
#######################################################################

date=$(date '+%Y-%m-%d %H:%M:%S')
echo "$date [ INFO ] :: DEPLOYING SAM PROJECT"
sam deploy \
  --profile ${AWS_PROFILE} \
  --template-file "template_${UUID}.yaml" \
  --stack-name "${ENV}-${STACK}-Infrastructure" \
  --capabilities CAPABILITY_NAMED_IAM \
  --tags Project=${PROJECT} \
  --s3-bucket ${BUCKET} \
  --parameter-overrides \
  UUID=${UUID} \
  Project=${PROJECT} \
  DeployBucket=${BUCKET} \
  Environment=${ENV} \
  LogLevel=${LOG_LEVEL} \
  RetentionInDays=${LOG_RETENTION_IN_DAYS} \
  OAuthToken=${O_AUTH_TOKEN_GITLAB} \
  Repository=${REPOSITORY}

#######################################################################
# LIMPIEZA DE PRECESO DE DESPLIEGUE
#######################################################################

rm "template_${UUID}.yaml"
rm -rf "$(pwd)/.aws-sam"
