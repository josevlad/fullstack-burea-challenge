'use strict';

require('/opt/custon-logger').overwriteSystemLogs();

const ApplicationException = require('./exceptions/application-exception');

const {
    awsDynamoConverter,
    checkEnvVariablesAreDefined,
    getEnvironmentVariable,
    buildResponse,
    isObject,
    isEmpty,
    isEqual
} = require("./utils");

const {
    DynamoDBClient,
    ElasticSearchClient
} = require('./clients');

module.exports = {
    checkEnvVariablesAreDefined,
    getEnvironmentVariable,
    ApplicationException,
    ElasticSearchClient,
    awsDynamoConverter,
    DynamoDBClient,
    buildResponse,
    isEmpty,
    isObject,
    isEqual
};