'use strict';

class ApplicationException extends Error {

    constructor(message, statusCode) {
        super();
        this.message = message || 'Application Exception';
        this.statusCode = statusCode || '500';
    }
}

module.exports = ApplicationException;
