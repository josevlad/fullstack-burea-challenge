'use strict';

const isEmpty = require("lodash.isempty");
const isEqual = require("lodash.isequal");
const isObject = require("lodash.isobject");
const AWS = require("aws-sdk");

const isEmptyWraper = (value) => {
    let typeofValue = typeof value;
    switch (typeofValue) {
        case "number":
            return !value;
        default:
            return isEmpty(value);
    }
};

const checkEnvVariablesAreDefined = (variableNames) => {
    if (isEmptyWraper(variableNames) || !Array.isArray(variableNames))
        throw new Error('NullPointerException: not variableNames list found');

    const environments = variableNames;

    environments.forEach(env => {
        const isEmpty = isEmptyWraper(process.env[env]);
        if (isEmpty) {
            throw new Error(`NullPointerException: ${env} variable is not defined`);
        }
    });
};

const getEnvironmentVariable = (nameEnv) => {
    const variable = process.env[nameEnv];
    if (!variable)
        throw new Error(`NullPointerException: ${nameEnv} variable is not defined`);

    return variable;
};

const buildResponse = (successInfo, errorInfo, response) => {
    console.log('BUILD_RESPONSE.PARAMS', { successInfo, errorInfo, response });

    return isEmptyWraper(response)
        ? { errorType: errorInfo.errorType, statusCode: errorInfo.statusCode, message: errorInfo.message }
        : {
            statusCode: successInfo.statusCode,
            headers: {
                "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": !isEmptyWraper(successInfo.allowMethods)
                    ? successInfo.allowMethods
                    : "*"
            },
            body: successInfo.message
        };
};

const HttpMethod = {
    GET: 'GET',
    PUT: 'PUT',
    POST: 'POST',
    PATCH: 'PATCH',
    DELETE: 'DELETE'
}

module.exports = {
    awsDynamoConverter: AWS.DynamoDB.Converter,
    checkEnvVariablesAreDefined,
    getEnvironmentVariable,
    isEmpty: isEmptyWraper,
    buildResponse,
    HttpMethod,
    isObject,
    isEqual
};