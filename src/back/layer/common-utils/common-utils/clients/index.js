
module.exports = {
    DynamoDBClient: require('./dynamo-client'),
    ElasticSearchClient: require('./elastic-search-client')
};