'use strict';

const AWS = require('aws-sdk');
const path = require('path');
const bodybuilder = require('bodybuilder');
const { getEnvironmentVariable, isEmpty, HttpMethod } = require('../utils');

class ElasticSearchClient {

    constructor(index, esNode) {
        this.client = {
            endpoint: esNode || getEnvironmentVariable('MARKDOWN_ELASTICSEARCH_URL'),
            enventsIndex: `${index}-events`,
            region: 'us-east-1',
            mappingType: index,
            index
        };
        this.endpoint = new AWS.Endpoint(this.client.endpoint);
        this.creds = new AWS.EnvironmentCredentials('AWS');
        this.hasOwnProperty = Object.prototype.hasOwnProperty;
    }

    async requestToES(index, action, body, method) {
        return new Promise((resolve, reject) => {
            let req = new AWS.HttpRequest(this.endpoint);

            req.method = method || HttpMethod.POST;
            req.path = encodeURI(path.join('/', index, action));
            req.region = this.client.region;
            req.headers['presigned-expires'] = false;
            req.headers['Host'] = this.endpoint.host;

            if (!isEmpty(body)) {
                req.body = JSON.stringify(body);
                req.headers['Content-Type'] = 'application/json';
                req.headers['Content-Length'] = Buffer.byteLength(req.body);
            }

            const signer = new AWS.Signers.V4(req, 'es');
            signer.addAuthorization(this.creds, new Date());

            new AWS.NodeHttpClient()
                .handleRequest(req, null,
                    res => {
                        let body = '';
                        res.on('data', chunk => {
                            body += chunk;
                        });
                        res.on('end', chunk => {
                            resolve(JSON.parse(body));
                        });
                    }, err => {
                        console.error('REQUEST_TO_ES.REQ.ERR', { err });
                        reject(err);
                    }
                );
        });
    }

    async insertOrUpdate(body, id) {
        try {
            console.log('ELASTIC_SEARCH_CLIENT.INSERT_OR_UPDATE:');
            console.log({ body, id, index: this.client.index });
            let response = await this.requestToES(this.client.index, `_doc/${id}`, body, HttpMethod.POST);

            console.log('ELASTIC_SEARCH_CLIENT.INSERT_OR_UPDATE.RES:');
            console.log({ element: body, id, index: this.client.index, response });
            return response;
        } catch (err) {
            console.error('INSERT_OR_UPDATE.ERROR', { err });
            throw err;
        }
    }

    async deleteById(id) {
        try {
            return await this.requestToES(this.client.index, `_doc/${id}`, null, HttpMethod.DELETE);
        } catch (err) {
            console.error('GET_BY_ID.ERROR', { err });
            throw err;
        }
    }

    async getById(id) {
        try {
            return await this.requestToES(this.client.index, `_doc/${id}/_source`, null, HttpMethod.GET);
        } catch (err) {
            console.error('GET_BY_ID.ERROR', { err });
            throw err;
        }
    }

    async getAll(page, limit) {
        page = isEmpty(page) ? 1 : page;
        limit = isEmpty(limit) ? 30 : limit;

        const offset = (page - 1) * limit;
        const queryBuilder = bodybuilder();
        const body = queryBuilder
            .from(offset)
            .size(limit)
            .build();

        try {
            const acction = '_search?filter_path=hits.hits._source,hits.total.value';
            let res = await this.requestToES(this.client.index, acction, body, HttpMethod.GET);

            console.log('GET_ALL.RES', { res });
            let {
                hits: {
                    hits,
                    total: {
                        value
                    }
                }
            } = res;

            return this.buildSetResult(hits, value, page, limit);
        } catch (err) {
            console.error('GET_ALL.ERROR', { err });
            throw err;
        }
    }

    buildSetResult(arrayResult, totalRecords, currenPage, limit) {
        console.log('ELASTIC_SEARCH_CLIENT.BUILD_SET_RESULT');
        console.log({ arrayResult, totalRecords, currenPage, limit });
        let response, records = [];
        if (isEmpty(arrayResult))
            response = { limit, currenPage, totalRecords, records };
        else {
            arrayResult.forEach(obj => {
                const [first] = Object.keys(obj);
                const element = obj[first];
                element.createAt = element.createAt.replace(/T/, ' ').replace(/\..+/, '');
                element.updateAt = element.updateAt.replace(/T/, ' ').replace(/\..+/, '');
                records.push(element);
            });
            response = { limit, currenPage, totalRecords, records };
        }
        console.log('ELASTIC_SEARCH_CLIENT.BUILD_SET_RESULT:RESPONSE');
        console.log({ response });
        return response
    }


}

module.exports = ElasticSearchClient;