'use strict';

const AWS = require("aws-sdk");
const { isEmpty, ApplicationException } = require('../utils');

class DynamoDBClient {

    constructor(tableName) {
        this._client = new AWS.DynamoDB.DocumentClient({ maxRetries: 1 });
        this._tableName = tableName;
    }

    async save(item, isUpdate = false) {
        console.log('SET_MARKDOWN_DDB', { item });
        try {
            let params = {
                TableName: this._tableName,
                ReturnValues: "ALL_OLD",
                Item: item
            };

            const today = new Date(Date.now()).toISOString();

            if (!isUpdate) {
                params.Item.updateAt = today;
                params.Item.createAt = today;
            } else
                params.Item.updateAt = today;

            let result = await this._client.put(params).promise();
            let status = isEmpty(result.Item)
                ? "saved"
                : "updated";
            return { item, status };
        } catch (err) {
            console.error('SET_MARKDOWN_DDB_ERR', { item, err });
            throw err;
        }
    }

    async getByKey(id) {
        console.log('GET_BY_KEY', { id });
        try {
            let params = {
                TableName: this._tableName,
                Key: { id }
            };
            const { Item: markdown } = await this._client.get(params).promise();
            console.log('MARKDOWN', { markdown });

            return markdown;
        } catch (err) {
            console.error('GET_BY_KEY_MARKDOWN_DDB_ERR', { id, err });
            throw err;
        }
    }

    async delete(id) {
        console.log('DELETE_MARKDOWN_DDB', { id });
        try {
            const params = {
                TableName: this._tableName,
                Key: { id: id }
            };
            const registry = await this._client.delete(params).promise();

            if (!isEmpty(registry)) {
                console.error('DELETE_MARKDOWN_DDB_ERR', { id, registry });
                throw new ApplicationException(`error to delete markdown with id ${id}`);
            }
        } catch (err) {
            console.error('DELETE_MARKDOWN_DDB_ERR', { id, err });
            throw err;
        }
    }

}

module.exports = DynamoDBClient;