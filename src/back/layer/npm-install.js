const fs = require('fs');
const resolve = require('path').resolve;
const join = require('path').join;
const cp = require('child_process');
const os = require('os');

// get library path
const layerPath = resolve(__dirname, './');

fs.readdirSync(layerPath).forEach((mod) => {
    const libPath = join(layerPath, mod, mod);

    // ensure path has package.json
    if (libPath.endsWith('.js') || !fs.existsSync(join(libPath, 'package.json'))) return;

    // npm binary based on OS
    const npmCmd = os.platform().startsWith('win') ? 'npm.cmd' : 'npm';

    // install folder
    cp.spawn(npmCmd, ['i', '--only=production'], {
        env: process.env,
        cwd: libPath,
        stdio: 'inherit'
    });
});