const { Log, CustomErrorLog } = require('../models');
const { logMethods } = require('./log-methods');
const {
    getObjectType,
    isError,
    isAWSException
} = require('../utils');
const { specialExceptions } = require('../constants');

const buildLog = (levels, level, filename, line) => {
    const objectType = getObjectType(levels[level]);
    const message = formatMessage(levels, level);
    const label = formatLabel(filename);
    return new Log(message, line, label, objectType);
};

const formatLabel = (filename) => {
    let parts = filename.split('/');
    return parts.length !== 1
        ? parts[parts.length - 2] + '/' + parts.pop()
        : filename;
};

const formatMessage = (levels, level) => {
    let message = levels[level];
    if (!isError(message)) {
        return logMethods[level](message);
    }
    return formatError(message).message;
};

/**
 *
 * @param message
 * @returns {{message: string}}
 */
const formatError = (message) => {
    let response;
    if (isAWSException(message)) {
        response = new CustomErrorLog(message);
        //response.filename = formatLabel();
        return {
            message: `${specialExceptions.label} ${JSON.stringify(
                response
            )}`
        };
    }
    response = message;
    return {
        message: response.message
            ? response.message
            : JSON.stringify(response)
    };
};

module.exports = {
    buildLog
};
