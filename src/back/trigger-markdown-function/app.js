'use strict';

const { TriggerServices } = require('./services');
require('/opt/custon-logger').overwriteSystemLogs();

exports.lambdaHandler = async (event, context, callback) => {
    try {
        return await new TriggerServices(event.Records).init();
    } catch (err) {
        console.error('LAMBDA_HANDLER_ERR', { event, err });
        callback(JSON.stringify(err));
    }
};
