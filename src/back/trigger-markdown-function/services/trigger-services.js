'use strict';

const {
    buildResponse,
    ElasticSearchClient,
    awsDynamoConverter,
    ApplicationException
} = require('/opt/common-utils');

class TriggerServices {

    constructor(records) {
        this.records = records;
        this.elasticSearchClient = new ElasticSearchClient('markdown');
        this.resultElasticSearch = [];
    }

    async init() {
        let response, successInfo, errorInfo, saved, markdown;
        try {
            for (const record of this.records) {
                switch (record.eventName) {
                    case 'INSERT':
                    case 'MODIFY':
                        markdown = awsDynamoConverter.unmarshall(record.dynamodb.NewImage);
                        saved = await this.elasticSearchClient.insertOrUpdate(markdown, markdown.id);
                        break;
                    case 'REMOVE':
                        markdown = awsDynamoConverter.unmarshall(record.dynamodb.OldImage);
                        saved = await this.elasticSearchClient.deleteById(markdown.id);
                        break;
                    default:
                        console.error('ERROR_IN_RECORD_FROM_DDB', JSON.stringify({ record }, null, 2));
                        throw new ApplicationException('error when proccessing the dynamo record');

                }
                this.resultElasticSearch.push(saved);
            }
            response = this.resultElasticSearch
            successInfo = { statusCode: 201, message: response }
        } catch (err) {
            console.error('TRIGGER_SERVICES.INIT.ERROR', { err });
            errorInfo = { statusCode: 409, message: err.toString() };
        }

        console.log('TRIGGER_SERVICES.INIT.RESULT', { successInfo, errorInfo, response });
        return buildResponse(successInfo, errorInfo, response);
    }
}

module.exports = TriggerServices;