'use strict';

const {
    buildResponse,
    DynamoDBClient,
    getEnvironmentVariable
} = require('/opt/common-utils');

const { v4: uuidv4 } = require('uuid');


class MarkdownService {

    constructor() {
        this.dynamoClient = new DynamoDBClient(getEnvironmentVariable('MARKDOWN_TABLE_DDB'));
        this.httpStatus = 409;
    }

    async run(body) {
        switch (body.method) {
            case 'CREATE':
                delete body.method;
                return this.createMarkdown(body);
            default:
                throw new Error(`UNDEFINED_METHOD ${body.method}`);
        }
    }

    async createMarkdown(body) {
        let response, successInfo, errorInfo;
        try {
            const { title, markdown } = body;
            const id = uuidv4();
            ({ item: response } = await this.dynamoClient.save({ id, title, markdown }));
            successInfo = { statusCode: 201, message: response };
        } catch (err) {
            console.error('MARKDOWN_SERVICE.CREATE_MARKDOWN.ERROR', { err });
            errorInfo = { statusCode: 409, message: err.toString() };
        }
        return buildResponse(successInfo, errorInfo, response);
    }
}

module.exports = MarkdownService;