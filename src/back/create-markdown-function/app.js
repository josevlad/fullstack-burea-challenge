'use strict';

const { MarkdownService } = require('./services');
require('/opt/custon-logger').overwriteSystemLogs();

exports.lambdaHandler = async (event, context, callback) => {
    console.debug('LAMBDA_HANDLER', { event });
    try {
        return await new MarkdownService().run(event);
    } catch (err) {
        console.error('LAMBDA_HANDLER_ERR', { event, err });
        callback(err);
    }
};
