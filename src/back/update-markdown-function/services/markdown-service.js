'use strict';

const {
    isEmpty,
    buildResponse,
    DynamoDBClient,
    ApplicationException,
    getEnvironmentVariable
} = require('/opt/common-utils');

class MarkdownService {

    constructor() {
        this.dynamoClient = new DynamoDBClient(getEnvironmentVariable('MARKDOWN_TABLE_DDB'));
        this.httpStatus = 409;
    }

    async run(body) {
        switch (body.method) {
            case 'UPDATE':
                delete body.method;
                return this.updateMarkdown(body);
            default:
                throw new Error(`UNDEFINED_METHOD ${body.method}`);
        }
    }

    async updateMarkdown(body) {
        let response, successInfo, errorInfo;
        try {
            const { title, markdown, id } = body;
            let byKey = await this.dynamoClient.getByKey(id);

            console.log('DELETE_MARKDOWN.BY_KEY', { byKey });
            if (isEmpty(byKey, null)) {
                this.httpStatus = 404;
                throw new ApplicationException(`Markdown with id ${id} does not exist`);
            }

            byKey.title = title;
            byKey.markdown = markdown;
            ({ item: response } = await this.dynamoClient.save(byKey, true));

            successInfo = { statusCode: 200, message: response };
        } catch (err) {
            console.error('DELETE_MARKDOWN.ERROR', { err });
            response = null;
            errorInfo = { statusCode: this.httpStatus, errorType: 'BadRequest', message: err.toString().split(':')[1] };
        }
        return buildResponse(successInfo, errorInfo, response);
    }
}

module.exports = MarkdownService;