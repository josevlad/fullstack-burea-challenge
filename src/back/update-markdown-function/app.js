'use strict';

const { MarkdownService } = require('./services');
require('/opt/custon-logger').overwriteSystemLogs();

exports.lambdaHandler = async (event, context, callback) => {
    console.log('LAMBDA_HANDLER', { event });
    try {
        let response = await new MarkdownService().run(event);
        if (response.statusCode === 200)
            return response;
        else {
            response.requestId = context.awsRequestId;
            let error = JSON.stringify(response);
            console.error('LAMBDA_HANDLER_ERR', { event, response });
            callback(error);
        }
    } catch (err) {
        console.error('LAMBDA_HANDLER_ERR', { event, err });
        callback(JSON.stringify(err));
    }
};
