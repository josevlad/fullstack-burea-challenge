'use strict';

const {
    isEmpty,
    buildResponse,
    DynamoDBClient,
    ApplicationException,
    getEnvironmentVariable
} = require('/opt/common-utils');

class MarkdownService {

    constructor() {
        const dynamoTable = getEnvironmentVariable('MARKDOWN_TABLE_DDB');
        this.dynamoClient = new DynamoDBClient(dynamoTable);
        this.httpStatus = 409;
    }

    async run(body, awsRequestId, cb) {
        switch (body.method) {
            case 'DELETE':
                delete body.method;
                return this.deleteMarkdown(body);
            default:
                throw new Error(`UNDEFINED_METHOD ${body.method}`);
        }
    }

    async deleteMarkdown(body) {
        let response, successInfo, errorInfo;
        try {
            const { id } = body;
            let byKey = await this.dynamoClient.getByKey(id);

            console.log('DELETE_MARKDOWN.BY_KEY', { byKey });
            if (isEmpty(byKey, null)) {
                this.httpStatus = 404;
                throw new ApplicationException(`Markdown with id ${id} does not exist`);
            }

            await this.dynamoClient.delete(id);
            response = {
                message: 'successful deletion',
                resourse: byKey
            }
            successInfo = { statusCode: 200, message: response };
        } catch (err) {
            console.error('DELETE_MARKDOWN.ERROR', { err });
            response = null;
            errorInfo = { statusCode: this.httpStatus, errorType: 'BadRequest', message: err.toString().split(':')[1] };
        }
        return buildResponse(successInfo, errorInfo, response);
    }
}

module.exports = MarkdownService;