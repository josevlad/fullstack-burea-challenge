'use strict';

const {
    isEmpty,
    buildResponse,
    ElasticSearchClient,
    ApplicationException
} = require('/opt/common-utils');


class MarkdownService {

    constructor() {
        this.elasticSearchClient = new ElasticSearchClient('markdown');
        this.httpStatus = 409;
    }

    async run(body) {
        switch (body.method) {
            case 'GET':
                delete body.method;
                return this.getMarkdown(body);
            default:
                throw new Error(`UNDEFINED_METHOD ${body.method}`);
        }
    }

    async getMarkdown(body) {
        let response = null, successInfo = null, errorInfo = null;
        try {
            response = isEmpty(body.id)
                ? await this.elasticSearchClient.getAll(body.page, body.limit)
                : await this.elasticSearchClient.getById(body.id);

            if (!isEmpty(response.error)) {
                console.log('GET_MARKDOWN.ERROR', { response });
                this.httpStatus = 404;
                throw new ApplicationException(`Markdown with id ${body.id} does not exist`);
            }

            console.log('GET_MARKDOWN.RESPONSE', { response });
            successInfo = { statusCode: 200, message: response, allowMethods: 'GET' };
        } catch (err) {
            console.error('GET_MARKDOWN.ERROR', { err });
            response = null;
            errorInfo = { statusCode: this.httpStatus, errorType: 'BadRequest', message: err.toString().split(':')[1] };
        }
        return buildResponse(successInfo, errorInfo, response);
    }
}

module.exports = MarkdownService;