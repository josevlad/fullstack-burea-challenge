import axios from "axios";
import { host } from '../api-rest-info.json';

class BackendServices {

    constructor() {
        this.axiosInstance = axios.create({
            baseURL: host,
            timeout: 10000
        });
    }

    save = async (path, body) => {
        try {
            const { data } = await this.axiosInstance.post(path, body);
            console.log(data);
            return data;
        } catch (error) {
            console.error(error);
        }
    }

    getAllData = async (path) => {
        try {
            const { data } = await this.axiosInstance.get(path);
            return data;
        } catch (error) {
            console.error(error);
        }
    }

    deleteById = async (path, id) => {
        try {
            const { data } = await this.axiosInstance.delete(`${path}/${id}`);
            return data;
        } catch (error) {
            console.error(error);
        }
    }

    update = async (path, id, body) => {
        try {
            const { data } = await this.axiosInstance.put(`${path}/${id}`, body);
            console.log(data);
            return data;
        } catch (error) {
            console.error(error);
        }
    }

}

export default BackendServices;