import React from 'react';

const Header = props => {

    const { onClickSaveButton, onClickClearBotton } = props;

    return (
        <div className="row">
            <div className="col-sm-3 ">
                <h1 className="">Markdown Editor</h1>
            </div>
            <div className="col-sm-9">
                <button
                    type="button"
                    className="btn btn-success mt-2"
                    onClick={() => onClickSaveButton()}>Save
                </button>

                <button
                    type="button"
                    className="btn btn-primary mt-2 ml-2"
                    onClick={() => onClickClearBotton()}>Clear fields
                </button>
            </div>
        </div>
    );
}

export default Header;