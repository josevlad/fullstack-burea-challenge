import React from 'react';
import MarkdownItem from "./MarkdownItem";

const MarkdownList = props => {

    const {
        markdownList,
        textEmptyList,
        onClickDeleteButtonMarkdown,
        onClickItemMarkdown
    } = props;

    const markdowns = props.markdownList.map(markdown =>
        <MarkdownItem
            onClickItemMarkdown={onClickItemMarkdown}
            key={markdown.id}
            doc={markdown}
            onClickDeleteButtonMarkdown={onClickDeleteButtonMarkdown}/>
    );

    return (
        <div className="col-sm">

            <div className="card">
                <div className="card-header">
                    <strong>Documents</strong>
                </div>
                <ul className="list-group" style={{
                    height: "550px",
                    marginBottom: "10px",
                    overflow: "scroll",
                    WebkitOverflowScrolling: "touch"
                }}>
                    {markdownList.length > 0
                        ? markdowns
                        : <li className="list-group-item">
                            {textEmptyList}
                        </li>
                    }

                </ul>
            </div>
        </div>


    );
}

export default MarkdownList;