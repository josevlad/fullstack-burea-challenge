import React from 'react';

const MarkdownForm = props => {
    const {
        txtTitle,
        markdownText,
        onStartWritingTitle,
        onStartWritingMarkdown } = props;

    return (
        <div className="col-sm-4">

            <div className="form-group mt-2">
                <input
                    id="mdTitle"
                    type="text"
                    className="form-control"
                    placeholder="Document title markdown..."
                    value={txtTitle}
                    onChange={(e) => onStartWritingTitle(e.target.value)}/>
            </div>

            <div className="form-group">
                <textarea
                    id="text"
                    className="form-control border"
                    style={{
                        width: "100%",
                        height: "100%",
                        resize: "none",
                        border: "none",
                        outline: "none",
                        fontSize: "17px"
                    }}
                    placeholder="###### Write your markdown text here..." rows="21"
                    value={markdownText}
                    onChange={(e) => onStartWritingMarkdown(e.target.value)}/>
            </div>

        </div>
    );
}

export default MarkdownForm;