import React from 'react';
import ReactMarkdown from 'react-markdown';

const MarkdownPreview = props => {
    const { markdownText } = props;
    return (
        <div className="col-sm-5">
            <div className="form-group">
                <div className="overflow-auto mt-2 p-3 mb-3 mb-md-0 mr-md-3 bg-light"
                     style={{ height: '37.4em' }}>
                    <ReactMarkdown source={markdownText}/>
                </div>
            </div>
        </div>
    );
}

export default MarkdownPreview;