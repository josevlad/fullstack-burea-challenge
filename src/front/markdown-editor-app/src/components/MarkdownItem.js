import React from 'react';

const MarkdownItem = props => {

    const onClickDeleteItem = (event) => props.onClickDeleteButtonMarkdown(event.target.id);
    const onClickItem = (event) => props.onClickItemMarkdown(props.doc);

    return (
        <div className="d-flex flex-row m-1">
            <div className="list-group-item list-group-item-action" onClick={onClickItem} doc={props.doc}>
                {props.doc.title}
            </div>
            <button
                className="btn-danger btn-sm ml-1"
                style={{ "float": "right" }}
                id={props.doc.id}
                onClick={onClickDeleteItem}>delete
            </button>
        </div>
    );
}

export default MarkdownItem;