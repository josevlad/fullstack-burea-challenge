import React from 'react';
import { encode, decode } from 'js-base64';
import { Header, MarkdownForm, MarkdownList, MarkdownPreview } from "./components";
import { BackendServices } from './utils';

class App extends React.Component {

    backendServices = new BackendServices();

    state = {
        markdownList: [],
        title: '',
        markdown: '',
        idToUpdate: null,
        textEmptyList: 'Loading data...'
    }

    async componentDidMount() {
        await this.loadMarkdownList();
    }

    loadMarkdownList = async () => {
        let state = { ...this.state };
        let res = await this.backendServices.getAllData('/markdowns');
        state.markdownList = res.body.records.reduce((arr, item) => {
            item.markdown = decode(item.markdown);
            arr.push(item);
            return arr;
        }, []);
        state.textEmptyList = 'No found data';
        this.setState(state);
    }

    deleteMarkdownById = async (id) => {
        if (window.confirm(`Would you like to eleminate this document? (${id})`)) {
            const { statusCode } = await this.backendServices.deleteById('/markdowns', id);
            if (statusCode === 200) {
                alert('the record has been successfully deleted');
                const { idToUpdate } = this.state;
                if (idToUpdate === id) this.clearData(true);
                await this.loadMarkdownList();
            } else {
                alert('oops! there was an error deleting the record');
            }
        }
    }

    saveData = async () => {
        const { title, markdown, idToUpdate } = this.state;
        if (!title || !markdown) {
            alert('title and/or markdown text cannot be empty')
            return;
        }

        if (idToUpdate === null) {
            const { statusCode, body } = await this.backendServices.save('/markdowns', { title, markdown: encode(markdown) });
            if (statusCode === 201) {
                alert('the record has been successfully saved');
                let state = { ...this.state };
                state.idToUpdate = body.id;
                this.setState(state);
            } else {
                alert('oops! there was an error saving the record');
            }
        } else {
            const { statusCode } = await this.backendServices.update('/markdowns', idToUpdate, {
                title,
                markdown: encode(markdown)
            });
            if (statusCode === 200) {
                alert('the record has been successfully update');
            } else {
                alert('oops! there was an error updating the record');
            }
        }
        await this.loadMarkdownList();
    }

    onWritingTitle = (text) => {
        let state = { ...this.state };
        state.title = text;
        this.setState(state);
    }

    onWritingMarkdown = (text) => {
        let state = { ...this.state };
        state.markdown = text;
        this.setState(state);
    }

    clearData = (isForced = false) => {
        const { markdown, title } = this.state;

        if (isForced || !!markdown || !!title) {
            if (window.confirm('Are you sure you want to clear the fields?')) {
                let state = { ...this.state };
                state.title = '';
                state.markdown = '';
                state.idToUpdate = null;
                this.setState(state)
            }
        }
    }

    loadDataInForm = (document) => {
        let state = { ...this.state };
        state.idToUpdate = document.id;
        state.title = document.title;
        state.markdown = document.markdown;
        this.setState(state);
    }

    render() {
        const { markdownList, textEmptyList, title, markdown } = this.state;
        const {
            deleteMarkdownById,
            saveData,
            clearData,
            onWritingTitle,
            onWritingMarkdown,
            loadDataInForm
        } = this;

        return (
            <main role="main" className="container-fluid">

                <Header
                    onClickSaveButton={saveData}
                    onClickClearBotton={clearData}/>

                <div className="row">

                    <MarkdownList
                        markdownList={markdownList}
                        textEmptyList={textEmptyList}
                        onClickDeleteButtonMarkdown={deleteMarkdownById}
                        onClickItemMarkdown={loadDataInForm}/>

                    <MarkdownForm
                        onStartWritingTitle={onWritingTitle}
                        onStartWritingMarkdown={onWritingMarkdown}
                        txtTitle={title}
                        markdownText={markdown}/>

                    <MarkdownPreview
                        markdownText={markdown}/>
                </div>
            </main>
        )
    }
}

export default App;
