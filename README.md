# Fullstack Burea Challenge

Proyecto Fullstack para el crud de texto de tipo markdown, realizado con nodejs, react y servicios de aws.

## Requerimientos

Para realizar el despliegue de la solucion, en necesario tener instalado lo siguiente:
* nodejs version 12
* una cuenta aws
* aws cli
* aws sam cli
* credenciales configuradas en la maquina desde donde se hace el despliegue (~/.aws/credentials)
* apikey de authenticacion a un respositorio git

## Despliegue

Para realizar el despliegue, se necesita hacer los siguientes pasos:

* Setear un variable de ambiente en el sistema operativo llamada **O_AUTH_TOKEN_GITLAB** con el valor de la apikey del repositorio git
* Configurar el nombre del **AWS_PROFILE** de aws cli en los archivos **deploy-infra.sh, deploy-persistence.sh y deploy-security.sh**
* Ejecutar dichos archivos en el siguiente orden: 
  * deploy-security.sh
  * deploy-persistence.sh
  * deploy-infra.sh
    
Ejemplo:

```bash
./scripts/deploy-security.sh
```

Eso empezara a tomar las definiciones de los archivos .yaml que estan en la carpeta cloudformation

## Uso

Al ir a la consola de AWS, ir al servicio de amplify y entrar en la app **dev-Markdown-MarkdownEditorReactApp**.
En caso que no se haya realizado el building, se puede ejecutar a mano en ese servicio.

Una vez realizado la publicacion, se puede acceder al dominio asignado para su acceso.

## Acceso a la aplicacion 

para ir al front y probar la solucion, acceder [aqui](https://master.d2kshhmi182ac.amplifyapp.com/)

## Grafico de arquitectura del proyecto

![Alt text](resources/microservices-architecture.png?raw=true "Optional Title")
